/* A Bison parser, made by GNU Bison 3.3.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2019 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.3.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 21 "parse.y" /* yacc.c:337  */

#include "config.h"

#include <glib.h>
#include <string.h>
#include "structs.h"
#include "mpwrap.h"
#include "eval.h"
#include "dict.h"
#include "util.h"
#include "calc.h"
#include "matrix.h"
#include "matrixw.h"
	
#include "parseutil.h"

extern GSList *gel_parsestack;

extern gboolean gel_return_ret; /*should the lexer return on \n*/

/* prototype for yylex */
int yylex(void);
void yyerror(char *);


#line 96 "y.tab.c" /* yacc.c:337  */
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    STARTTOK = 258,
    LOADFILE = 259,
    LOADFILE_GLOB = 260,
    LOAD_PLUGIN = 261,
    CHANGEDIR = 262,
    PWD = 263,
    LS = 264,
    LS_ARG = 265,
    HELP = 266,
    HELP_ARG = 267,
    NUMBER = 268,
    STRING = 269,
    FUNCID = 270,
    FUNCTION = 271,
    CALL = 272,
    THREEDOTS = 273,
    PARAMETER = 274,
    RETURNTOK = 275,
    BAILOUT = 276,
    EXCEPTION = 277,
    CONTINUE = 278,
    BREAK = 279,
    LOCAL = 280,
    WHILE = 281,
    UNTIL = 282,
    FOR = 283,
    SUM = 284,
    PROD = 285,
    DO = 286,
    IF = 287,
    THEN = 288,
    ELSE = 289,
    TO = 290,
    BY = 291,
    IN = 292,
    AT = 293,
    MAKEIMAGPARENTH = 294,
    SEPAR = 295,
    NEXTROW = 296,
    EQUALS = 297,
    DEFEQUALS = 298,
    SWAPWITH = 299,
    TRANSPOSE = 300,
    ELTELTDIV = 301,
    ELTELTMUL = 302,
    ELTELTPLUS = 303,
    ELTELTMINUS = 304,
    ELTELTEXP = 305,
    ELTELTMOD = 306,
    DOUBLEFACT = 307,
    EQ_CMP = 308,
    NE_CMP = 309,
    CMP_CMP = 310,
    LT_CMP = 311,
    GT_CMP = 312,
    LE_CMP = 313,
    GE_CMP = 314,
    LOGICAL_XOR = 315,
    LOGICAL_OR = 316,
    LOGICAL_AND = 317,
    LOGICAL_NOT = 318,
    INCREMENT = 319,
    LOWER_THAN_ELSE = 320,
    LOWER_THAN_INCREMENT = 321,
    MOD = 322,
    ELTELTBACKDIV = 323,
    UMINUS = 324,
    UPLUS = 325
  };
#endif
/* Tokens.  */
#define STARTTOK 258
#define LOADFILE 259
#define LOADFILE_GLOB 260
#define LOAD_PLUGIN 261
#define CHANGEDIR 262
#define PWD 263
#define LS 264
#define LS_ARG 265
#define HELP 266
#define HELP_ARG 267
#define NUMBER 268
#define STRING 269
#define FUNCID 270
#define FUNCTION 271
#define CALL 272
#define THREEDOTS 273
#define PARAMETER 274
#define RETURNTOK 275
#define BAILOUT 276
#define EXCEPTION 277
#define CONTINUE 278
#define BREAK 279
#define LOCAL 280
#define WHILE 281
#define UNTIL 282
#define FOR 283
#define SUM 284
#define PROD 285
#define DO 286
#define IF 287
#define THEN 288
#define ELSE 289
#define TO 290
#define BY 291
#define IN 292
#define AT 293
#define MAKEIMAGPARENTH 294
#define SEPAR 295
#define NEXTROW 296
#define EQUALS 297
#define DEFEQUALS 298
#define SWAPWITH 299
#define TRANSPOSE 300
#define ELTELTDIV 301
#define ELTELTMUL 302
#define ELTELTPLUS 303
#define ELTELTMINUS 304
#define ELTELTEXP 305
#define ELTELTMOD 306
#define DOUBLEFACT 307
#define EQ_CMP 308
#define NE_CMP 309
#define CMP_CMP 310
#define LT_CMP 311
#define GT_CMP 312
#define LE_CMP 313
#define GE_CMP 314
#define LOGICAL_XOR 315
#define LOGICAL_OR 316
#define LOGICAL_AND 317
#define LOGICAL_NOT 318
#define INCREMENT 319
#define LOWER_THAN_ELSE 320
#define LOWER_THAN_INCREMENT 321
#define MOD 322
#define ELTELTBACKDIV 323
#define UMINUS 324
#define UPLUS 325

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 47 "parse.y" /* yacc.c:352  */

	mpw_t val;
	char *id;

#line 284 "y.tab.c" /* yacc.c:352  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */



#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  48
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   2515

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  93
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  11
/* YYNRULES -- Number of rules.  */
#define YYNRULES  130
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  310

#define YYUNDEFTOK  2
#define YYMAXUTOK   325

/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  ((unsigned) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      81,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    80,     2,     2,     2,    75,    91,    76,
      82,    83,    71,    69,    85,    70,    92,    72,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    68,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    86,    73,    87,    79,     2,    88,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    89,    84,    90,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    74,    77,    78
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   121,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   137,   138,   142,   146,
     147,   149,   150,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   177,   178,   179,   180,   181,   182,   184,   185,
     186,   187,   192,   193,   194,   195,   196,   197,   198,   199,
     201,   209,   211,   212,   213,   214,   215,   216,   217,   218,
     225,   230,   231,   232,   233,   234,   235,   236,   237,   238,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   249,
     250,   251,   252,   254,   255,   257,   258,   259,   261,   262,
     263,   264,   265,   266,   267,   268,   269,   270,   272,   273,
     276,   279,   282,   292,   293,   296,   304,   312,   320,   328,
     336,   345,   353,   361,   371,   372,   375,   378,   379,   382,
     383
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "STARTTOK", "LOADFILE", "LOADFILE_GLOB",
  "LOAD_PLUGIN", "CHANGEDIR", "PWD", "LS", "LS_ARG", "HELP", "HELP_ARG",
  "NUMBER", "STRING", "FUNCID", "FUNCTION", "CALL", "THREEDOTS",
  "PARAMETER", "RETURNTOK", "BAILOUT", "EXCEPTION", "CONTINUE", "BREAK",
  "LOCAL", "WHILE", "UNTIL", "FOR", "SUM", "PROD", "DO", "IF", "THEN",
  "ELSE", "TO", "BY", "IN", "AT", "MAKEIMAGPARENTH", "SEPAR", "NEXTROW",
  "EQUALS", "DEFEQUALS", "SWAPWITH", "TRANSPOSE", "ELTELTDIV", "ELTELTMUL",
  "ELTELTPLUS", "ELTELTMINUS", "ELTELTEXP", "ELTELTMOD", "DOUBLEFACT",
  "EQ_CMP", "NE_CMP", "CMP_CMP", "LT_CMP", "GT_CMP", "LE_CMP", "GE_CMP",
  "LOGICAL_XOR", "LOGICAL_OR", "LOGICAL_AND", "LOGICAL_NOT", "INCREMENT",
  "LOWER_THAN_ELSE", "LOWER_THAN_INCREMENT", "MOD", "':'", "'+'", "'-'",
  "'*'", "'/'", "'\\\\'", "ELTELTBACKDIV", "'%'", "'\\''", "UMINUS",
  "UPLUS", "'^'", "'!'", "'\\n'", "'('", "')'", "'|'", "','", "'['", "']'",
  "'`'", "'{'", "'}'", "'&'", "'.'", "$accept", "fullexpr", "expr",
  "deref", "ident", "paramdef", "anyequals", "funcdef", "identlist",
  "exprlist", "matrixrows", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,    58,    43,
      45,    42,    47,    92,   323,    37,    39,   324,   325,    94,
      33,    10,    40,    41,   124,    44,    91,    93,    96,   123,
     125,    38,    46
};
# endif

#define YYPACT_NINF -72

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-72)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  (!!((Yytable_value) == (-1)))

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      65,   -69,   300,    40,   -72,   -31,   -18,     4,     8,    21,
      44,    50,    51,    76,   -72,   -72,   -72,    -8,    42,   820,
     -72,   -72,   -72,   -72,    -4,   820,   820,    42,    42,    42,
     820,   820,   820,   820,   820,   820,    42,   -72,   820,   820,
     820,   -10,   820,    42,   -72,  1198,    -9,     2,   -72,   -72,
     -72,   -72,   -72,   -72,   -72,   -72,   -72,   -72,     0,    79,
     -72,    35,   -72,  2135,   131,   -72,   -24,  1442,  1488,     5,
      12,    85,  1258,  1536,  2224,  1996,   127,   127,   -72,   968,
      82,   921,  2042,    87,   -28,   820,   820,   -72,   -72,   -71,
     -72,   100,   330,   440,   820,   820,   820,   -72,   820,   820,
     820,   820,   820,   820,   -72,   820,   820,   820,   820,   820,
     820,   820,   820,   820,   820,   820,   820,   820,   820,   820,
     820,   820,   820,   820,   -72,   820,   -72,   -72,   520,   550,
     -21,   -14,   -72,   -72,   -72,   820,   820,   820,   820,    42,
     820,   820,   820,   820,   820,   820,   820,   820,   820,   820,
     820,   820,   -72,   630,   -72,   710,   -72,   710,   820,   -72,
     -23,   -65,   -72,   740,     3,   820,   875,   -72,  2135,  2267,
    2267,  2310,   124,   124,   631,   631,   127,   124,  2396,  2396,
    2353,  2396,  2396,  2396,  2396,  2181,  2181,  2224,  2267,  2435,
     631,   631,   124,   124,   124,   124,   124,   127,   -72,    41,
     -72,    69,    -7,   820,    92,   -16,  2267,  2135,  2135,   -72,
    2135,  2135,  1582,  1628,  1674,  1720,  1766,  1812,  2135,  2135,
    2089,  2135,   -72,   820,  1014,  2042,    87,   -72,   -72,   -72,
      75,   -72,   820,  1060,   -72,   410,   -72,   -72,    35,    81,
    2135,     9,    -6,   820,   820,   820,   820,   820,   820,   820,
     820,  2042,   -72,   -72,  1106,   -72,   101,   -72,  1152,   820,
      35,    -5,   820,    35,    93,  2135,  2135,  1304,  2135,  1350,
    2135,  1396,  2135,   -72,   -72,   -72,  2135,   820,    35,    96,
    2135,   820,    35,   820,   820,   820,   820,   820,   820,  2135,
     820,    35,  2135,   820,  2135,  1858,  2135,  1904,  2135,  1950,
    2135,   820,  2135,   820,   820,   820,  2135,  2135,  2135,  2135
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    15,     0,     0,    14,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   107,   108,   111,     0,     0,     0,
     103,   104,   105,   106,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    12,     0,     0,
       0,     0,     0,     0,   109,     0,    91,    88,     1,     3,
       4,    11,     5,    10,     6,     7,     8,     9,     0,     0,
     100,     0,    99,   102,     0,   125,     0,     0,     0,     0,
       0,     0,     0,     0,    51,    25,    57,    56,   110,   128,
       0,     0,   128,   130,     0,     0,     0,    89,   101,     0,
      90,     0,     0,     0,     0,     0,     0,    55,     0,     0,
       0,     0,     0,     0,    53,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    54,     0,    52,     2,     0,     0,
       0,     0,    98,   113,   114,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    22,     0,    21,     0,    28,     0,     0,    68,
       0,     0,    71,     0,     0,     0,     0,    13,    16,    23,
      24,    27,    36,    34,    30,    32,    59,    40,    42,    43,
      41,    44,    45,    46,    47,    50,    49,    48,    19,    60,
      29,    31,    33,    35,    37,    38,    39,    58,    94,     0,
      92,     0,     0,     0,     0,     0,   112,    17,    18,   124,
      73,    74,     0,     0,     0,     0,     0,     0,    75,    76,
      86,    26,    20,     0,   126,   126,   129,    69,    72,    97,
       0,    61,     0,     0,    62,     0,    95,    93,     0,     0,
     117,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   127,    70,    96,     0,    66,     0,    64,     0,     0,
       0,     0,     0,     0,     0,   115,    79,     0,    82,     0,
      85,     0,    87,    67,    65,    63,   123,     0,     0,     0,
     116,     0,     0,     0,     0,     0,     0,     0,     0,   120,
       0,     0,   121,     0,    77,     0,    80,     0,    83,     0,
     122,     0,   118,     0,     0,     0,   119,    78,    81,    84
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -72,   -72,    -2,   -72,    17,   -72,   -68,    -3,   -52,     1,
     102
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     3,    82,    46,    47,    62,   135,    60,    66,    83,
      84
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      45,   143,   145,   147,   204,    16,   131,    16,    16,    16,
      16,    16,     4,   158,   157,    16,   138,    63,   158,   162,
     157,   133,   134,    67,    68,   228,   133,   134,    72,    73,
      74,    75,    76,    77,    59,    61,    79,    81,    88,    80,
      48,    65,   142,    89,    69,    70,    71,   133,   134,   144,
      49,   133,   134,    78,   133,   134,   132,    16,    87,   159,
      90,   139,   203,    50,   227,   202,     1,    64,     2,   205,
     242,   139,    58,   128,    58,    65,    85,   133,   134,    86,
     238,   263,   278,   130,   129,    51,   231,   161,   232,    52,
     166,   168,   169,   170,   171,   261,   172,   173,   174,   175,
     176,   177,    53,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   146,   197,   236,    54,   157,   133,   134,   199,
     201,    55,    56,   206,   168,   207,   208,   243,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   221,
     239,   168,   237,   224,   157,   225,   209,    57,   253,   226,
     157,    58,    92,   233,   230,    92,   139,   155,   260,    97,
     259,   137,   157,   262,   102,   241,   104,   102,   139,   104,
     282,   139,   163,   291,   274,     0,     0,   160,     0,     0,
     264,     0,   277,     0,     0,   281,     0,     0,     0,     0,
     124,   240,     0,   125,   126,     0,   125,   126,     0,   279,
     290,     0,     0,     0,   293,     0,     0,     0,     0,    65,
       0,   251,     0,   301,     0,     0,     0,     0,     0,     0,
     254,     0,     0,   258,     0,     0,     0,     0,     0,     0,
       0,   265,   266,   267,   268,   269,   270,   271,   272,     0,
       0,     0,     0,     0,     0,     0,     0,   276,     0,    65,
     280,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   289,     0,     0,    65,   292,
       0,   294,   295,   296,   297,   298,   299,     0,   300,     0,
       0,   302,     0,     0,     0,     0,     0,     0,     0,   306,
       0,   307,   308,   309,     5,     6,     7,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    37,    38,     0,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    32,    33,     0,     0,     0,   164,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,     0,    39,   165,    40,     0,    41,    42,
       0,    43,    44,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,     0,     0,     0,   256,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,   257,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   167,    38,     0,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,   198,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,   200,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,    92,
       0,     0,     0,     0,     0,     0,    97,    98,    99,     0,
       0,   102,   103,   104,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    32,    33,     0,     0,     0,     0,    34,
      35,    36,   119,   120,   121,   122,   123,   124,     0,     0,
     125,   126,    38,   222,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,   223,     0,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,     0,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    32,    33,     0,     0,     0,     0,    34,
      35,    36,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,   229,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    14,    15,    16,    17,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    32,    33,     0,     0,     0,     0,    34,
      35,    36,    91,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    38,     0,    39,     0,    40,     0,    41,    42,
       0,    43,    44,    92,     0,   136,     0,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,    91,     0,
       0,     0,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,     0,     0,   125,   126,     0,     0,   234,    92,
     235,   136,     0,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,     0,    91,     0,     0,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,     0,     0,
     125,   126,     0,     0,     0,   156,    92,   152,   153,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,    91,     0,     0,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,     0,     0,   125,   126,     0,
       0,   154,    92,     0,   136,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,    91,     0,     0,
       0,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,     0,     0,   125,   126,     0,     0,   252,    92,     0,
     136,     0,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,    91,     0,     0,     0,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,     0,     0,   125,
     126,     0,     0,   255,    92,     0,   136,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,    91,
       0,     0,     0,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,     0,     0,   125,   126,     0,     0,   273,
      92,     0,   136,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,    91,     0,     0,     0,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,     0,
       0,   125,   126,     0,     0,   275,    92,     0,    93,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,     0,     0,     0,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,    91,     0,   125,   126,   127,
       0,     0,     0,     0,   148,   149,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    92,     0,   136,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,    91,     0,     0,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   283,     0,   125,   126,     0,
     284,     0,    92,     0,   136,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,    91,     0,     0,
       0,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   285,     0,   125,   126,     0,   286,     0,    92,     0,
     136,     0,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,    91,     0,     0,     0,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   287,     0,   125,
     126,     0,   288,     0,    92,     0,   136,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,    91,
       0,     0,     0,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   140,     0,   125,   126,     0,     0,     0,
      92,     0,   136,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,    91,     0,     0,     0,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   141,
       0,   125,   126,     0,     0,     0,    92,     0,   136,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,     0,     0,    91,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,     0,     0,   125,   126,   150,
       0,     0,     0,     0,    92,     0,   136,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,    91,
       0,     0,     0,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   244,     0,   125,   126,     0,     0,     0,
      92,     0,   136,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,    91,     0,     0,     0,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,     0,
       0,   125,   126,   245,     0,     0,    92,     0,   136,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,    91,     0,     0,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   246,     0,   125,   126,     0,
       0,     0,    92,     0,   136,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,    91,     0,     0,
       0,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,     0,     0,   125,   126,   247,     0,     0,    92,     0,
     136,     0,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,    91,     0,     0,     0,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   248,     0,   125,
     126,     0,     0,     0,    92,     0,   136,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,    91,
       0,     0,     0,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,     0,     0,   125,   126,   249,     0,     0,
      92,     0,   136,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,    91,     0,     0,     0,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   303,
       0,   125,   126,     0,     0,     0,    92,     0,   136,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,    91,     0,     0,     0,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   304,     0,   125,   126,     0,
       0,     0,    92,     0,   136,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,    91,     0,     0,
       0,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   305,     0,   125,   126,     0,     0,     0,    92,     0,
     136,     0,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,    91,     0,     0,     0,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,     0,     0,   125,
     126,     0,   151,     0,    92,     0,     0,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,    91,
       0,     0,     0,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,     0,     0,   125,   126,     0,     0,     0,
      92,     0,   136,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,     0,    91,     0,     0,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,     0,
       0,   125,   126,   250,     0,     0,     0,    92,     0,     0,
       0,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,    91,     0,     0,     0,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,     0,     0,   125,   126,
       0,     0,     0,    92,     0,     0,     0,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,    91,     0,
       0,     0,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,     0,     0,   125,   126,     0,     0,     0,    92,
       0,     0,     0,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,    91,     0,   114,     0,     0,     0,     0,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,     0,     0,
     125,   126,    92,     0,     0,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,    91,     0,     0,     0,     0,     0,
       0,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,     0,     0,   125,   126,    92,     0,     0,     0,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,    91,     0,     0,
       0,     0,     0,     0,     0,   116,   117,   118,   119,   120,
     121,   122,   123,   124,     0,     0,   125,   126,    92,     0,
       0,     0,     0,     0,    -1,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
      91,     0,     0,     0,     0,     0,     0,     0,   116,   117,
     118,   119,   120,   121,   122,   123,   124,     0,     0,   125,
     126,    92,     0,     0,     0,     0,     0,     0,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,    -1,   108,
     109,   110,   111,    91,     0,     0,     0,     0,     0,     0,
       0,   116,   117,   118,   119,   120,   121,   122,   123,   124,
       0,     0,   125,   126,    92,     0,     0,     0,     0,     0,
       0,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,     0,   108,   109,   110,   111,     0,     0,     0,     0,
       0,     0,     0,     0,   116,   117,   118,   119,   120,   121,
     122,   123,   124,    92,     0,   125,   126,     0,     0,     0,
      97,    98,    99,   100,   101,   102,   103,   104,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   116,   117,   118,   119,   120,   121,   122,
     123,   124,     0,     0,   125,   126
};

static const yytype_int16 yycheck[] =
{
       2,    69,    70,    71,    18,    15,    58,    15,    15,    15,
      15,    15,    81,    41,    85,    15,    40,    19,    41,    90,
      85,    42,    43,    25,    26,    90,    42,    43,    30,    31,
      32,    33,    34,    35,    17,    18,    38,    39,    41,    38,
       0,    24,    37,    42,    27,    28,    29,    42,    43,    37,
      81,    42,    43,    36,    42,    43,    59,    15,    41,    87,
      43,    85,   130,    81,    87,    86,     1,    71,     3,    83,
      86,    85,    82,    82,    82,    58,    86,    42,    43,    89,
      87,    87,    87,    83,    82,    81,    83,    86,    85,    81,
      92,    93,    94,    95,    96,    86,    98,    99,   100,   101,
     102,   103,    81,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,    37,   125,    83,    81,    85,    42,    43,   128,
     129,    81,    81,   135,   136,   137,   138,   205,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     202,   153,    83,   155,    85,   157,   139,    81,    83,   158,
      85,    82,    38,   165,   163,    38,    85,    85,    87,    45,
     238,    40,    85,   241,    50,    83,    52,    50,    85,    52,
      87,    85,    82,    87,    83,    -1,    -1,    85,    -1,    -1,
     242,    -1,   260,    -1,    -1,   263,    -1,    -1,    -1,    -1,
      76,   203,    -1,    79,    80,    -1,    79,    80,    -1,   261,
     278,    -1,    -1,    -1,   282,    -1,    -1,    -1,    -1,   202,
      -1,   223,    -1,   291,    -1,    -1,    -1,    -1,    -1,    -1,
     232,    -1,    -1,   235,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   243,   244,   245,   246,   247,   248,   249,   250,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   259,    -1,   242,
     262,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   277,    -1,    -1,   261,   281,
      -1,   283,   284,   285,   286,   287,   288,    -1,   290,    -1,
      -1,   293,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   301,
      -1,   303,   304,   305,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    81,    82,    -1,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    63,    64,    -1,    -1,    -1,    68,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    -1,    84,    85,    86,    -1,    88,    89,
      -1,    91,    92,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    63,    64,    -1,    -1,    -1,    68,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    83,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    81,    82,    -1,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    83,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    83,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      -1,    50,    51,    52,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    71,    72,    73,    74,    75,    76,    -1,    -1,
      79,    80,    82,    83,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    41,    -1,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    -1,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    83,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    13,    14,    15,    16,    -1,    -1,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    63,    64,    -1,    -1,    -1,    -1,    69,
      70,    71,    17,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    82,    -1,    84,    -1,    86,    -1,    88,    89,
      -1,    91,    92,    38,    -1,    40,    -1,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    17,    -1,
      -1,    -1,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    -1,    -1,    79,    80,    -1,    -1,    83,    38,
      85,    40,    -1,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    -1,    17,    -1,    -1,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    -1,    -1,
      79,    80,    -1,    -1,    -1,    84,    38,    39,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    17,    -1,    -1,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    -1,    -1,    79,    80,    -1,
      -1,    83,    38,    -1,    40,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    17,    -1,    -1,
      -1,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    -1,    -1,    79,    80,    -1,    -1,    83,    38,    -1,
      40,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    17,    -1,    -1,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    -1,    -1,    79,
      80,    -1,    -1,    83,    38,    -1,    40,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    17,
      -1,    -1,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    -1,    -1,    79,    80,    -1,    -1,    83,
      38,    -1,    40,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    17,    -1,    -1,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    -1,
      -1,    79,    80,    -1,    -1,    83,    38,    -1,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    -1,    -1,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    17,    -1,    79,    80,    81,
      -1,    -1,    -1,    -1,    26,    27,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    17,    -1,    -1,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    31,    -1,    79,    80,    -1,
      36,    -1,    38,    -1,    40,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    17,    -1,    -1,
      -1,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    31,    -1,    79,    80,    -1,    36,    -1,    38,    -1,
      40,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    17,    -1,    -1,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    31,    -1,    79,
      80,    -1,    36,    -1,    38,    -1,    40,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    17,
      -1,    -1,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    31,    -1,    79,    80,    -1,    -1,    -1,
      38,    -1,    40,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    17,    -1,    -1,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    31,
      -1,    79,    80,    -1,    -1,    -1,    38,    -1,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    -1,    17,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    -1,    -1,    79,    80,    33,
      -1,    -1,    -1,    -1,    38,    -1,    40,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    17,
      -1,    -1,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    31,    -1,    79,    80,    -1,    -1,    -1,
      38,    -1,    40,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    17,    -1,    -1,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    -1,
      -1,    79,    80,    35,    -1,    -1,    38,    -1,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    17,    -1,    -1,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    31,    -1,    79,    80,    -1,
      -1,    -1,    38,    -1,    40,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    17,    -1,    -1,
      -1,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    -1,    -1,    79,    80,    35,    -1,    -1,    38,    -1,
      40,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    17,    -1,    -1,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    31,    -1,    79,
      80,    -1,    -1,    -1,    38,    -1,    40,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    17,
      -1,    -1,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    -1,    -1,    79,    80,    35,    -1,    -1,
      38,    -1,    40,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    17,    -1,    -1,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    31,
      -1,    79,    80,    -1,    -1,    -1,    38,    -1,    40,    -1,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    17,    -1,    -1,    -1,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    31,    -1,    79,    80,    -1,
      -1,    -1,    38,    -1,    40,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    17,    -1,    -1,
      -1,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    31,    -1,    79,    80,    -1,    -1,    -1,    38,    -1,
      40,    -1,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    17,    -1,    -1,    -1,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    -1,    -1,    79,
      80,    -1,    36,    -1,    38,    -1,    -1,    -1,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    17,
      -1,    -1,    -1,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    -1,    -1,    79,    80,    -1,    -1,    -1,
      38,    -1,    40,    -1,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    -1,    17,    -1,    -1,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    -1,
      -1,    79,    80,    34,    -1,    -1,    -1,    38,    -1,    -1,
      -1,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    17,    -1,    -1,    -1,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    -1,    -1,    79,    80,
      -1,    -1,    -1,    38,    -1,    -1,    -1,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    17,    -1,
      -1,    -1,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    -1,    -1,    79,    80,    -1,    -1,    -1,    38,
      -1,    -1,    -1,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    17,    -1,    62,    -1,    -1,    -1,    -1,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    -1,    -1,
      79,    80,    38,    -1,    -1,    -1,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    17,    -1,    -1,    -1,    -1,    -1,
      -1,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    -1,    -1,    79,    80,    38,    -1,    -1,    -1,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    17,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    -1,    -1,    79,    80,    38,    -1,
      -1,    -1,    -1,    -1,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      17,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    -1,    -1,    79,
      80,    38,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    17,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      -1,    -1,    79,    80,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    -1,    56,    57,    58,    59,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    38,    -1,    79,    80,    -1,    -1,    -1,
      45,    46,    47,    48,    49,    50,    51,    52,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    -1,    -1,    79,    80
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     1,     3,    94,    81,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    63,    64,    69,    70,    71,    81,    82,    84,
      86,    88,    89,    91,    92,    95,    96,    97,     0,    81,
      81,    81,    81,    81,    81,    81,    81,    81,    82,    97,
     100,    97,    98,    95,    71,    97,   101,    95,    95,    97,
      97,    97,    95,    95,    95,    95,    95,    95,    97,    95,
     102,    95,    95,   102,   103,    86,    89,    97,   100,   102,
      97,    17,    38,    40,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    79,    80,    81,    82,    82,
      83,   101,   100,    42,    43,    99,    40,    40,    40,    85,
      31,    31,    37,    99,    37,    99,    37,    99,    26,    27,
      33,    36,    39,    40,    83,    85,    84,    85,    41,    87,
     103,   102,    90,    82,    68,    85,    95,    81,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    83,   102,
      83,   102,    86,    99,    18,    83,    95,    95,    95,    97,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    83,    41,    95,    95,   102,    87,    90,    83,
     102,    83,    85,    95,    83,    85,    83,    83,    87,   101,
      95,    83,    86,    99,    31,    35,    31,    35,    31,    35,
      34,    95,    83,    83,    95,    83,    68,    83,    95,    99,
      87,    86,    99,    87,   101,    95,    95,    95,    95,    95,
      95,    95,    95,    83,    83,    83,    95,    99,    87,   101,
      95,    99,    87,    31,    36,    31,    36,    31,    36,    95,
      99,    87,    95,    99,    95,    95,    95,    95,    95,    95,
      95,    99,    95,    31,    31,    31,    95,    95,    95,    95
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    93,    94,    94,    94,    94,    94,    94,    94,    94,
      94,    94,    94,    94,    94,    94,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      96,    97,    98,    99,    99,   100,   100,   100,   100,   100,
     100,   100,   100,   100,   101,   101,   102,   102,   102,   103,
     103
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     2,     4,     2,     1,     3,     4,     4,     3,
       4,     3,     3,     3,     3,     2,     4,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     2,     2,     2,     2,     2,     2,     2,     3,     3,
       3,     4,     4,     6,     5,     6,     5,     6,     3,     4,
       5,     3,     4,     4,     4,     4,     4,     8,    10,     6,
       8,    10,     6,     8,    10,     6,     4,     6,     1,     2,
       2,     1,     3,     4,     3,     4,     5,     4,     3,     2,
       2,     2,     2,     1,     1,     1,     1,     1,     1,     1,
       2,     1,     3,     1,     1,     5,     6,     4,     8,     9,
       7,     7,     8,     6,     3,     1,     3,     4,     1,     3,
       1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return (YYSIZE_T) (yystpcpy (yyres, yystr) - yyres);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yynewstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  *yyssp = (yytype_int16) yystate;

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = (YYSIZE_T) (yyssp - yyss + 1);

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 121 "parse.y" /* yacc.c:1652  */
    { YYACCEPT; }
#line 2027 "y.tab.c" /* yacc.c:1652  */
    break;

  case 3:
#line 122 "parse.y" /* yacc.c:1652  */
    { gel_command = GEL_LOADFILE; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2033 "y.tab.c" /* yacc.c:1652  */
    break;

  case 4:
#line 123 "parse.y" /* yacc.c:1652  */
    { gel_command = GEL_LOADFILE_GLOB; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2039 "y.tab.c" /* yacc.c:1652  */
    break;

  case 5:
#line 124 "parse.y" /* yacc.c:1652  */
    { gel_command = GEL_CHANGEDIR; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2045 "y.tab.c" /* yacc.c:1652  */
    break;

  case 6:
#line 125 "parse.y" /* yacc.c:1652  */
    { gel_command = GEL_LS; YYACCEPT; }
#line 2051 "y.tab.c" /* yacc.c:1652  */
    break;

  case 7:
#line 126 "parse.y" /* yacc.c:1652  */
    { gel_command = GEL_LS_ARG; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2057 "y.tab.c" /* yacc.c:1652  */
    break;

  case 8:
#line 127 "parse.y" /* yacc.c:1652  */
    { gel_command = GEL_HELP; YYACCEPT; }
#line 2063 "y.tab.c" /* yacc.c:1652  */
    break;

  case 9:
#line 128 "parse.y" /* yacc.c:1652  */
    { gel_command = GEL_HELP_ARG; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2069 "y.tab.c" /* yacc.c:1652  */
    break;

  case 10:
#line 129 "parse.y" /* yacc.c:1652  */
    { gel_command = GEL_PWD; YYACCEPT; }
#line 2075 "y.tab.c" /* yacc.c:1652  */
    break;

  case 11:
#line 130 "parse.y" /* yacc.c:1652  */
    { gel_command = GEL_LOADPLUGIN; gel_command_arg = (yyvsp[-1].id); YYACCEPT; }
#line 2081 "y.tab.c" /* yacc.c:1652  */
    break;

  case 12:
#line 131 "parse.y" /* yacc.c:1652  */
    { YYACCEPT; }
#line 2087 "y.tab.c" /* yacc.c:1652  */
    break;

  case 13:
#line 132 "parse.y" /* yacc.c:1652  */
    { gp_push_null(); PUSH_ACT(GEL_E_SEPAR); YYACCEPT; }
#line 2093 "y.tab.c" /* yacc.c:1652  */
    break;

  case 14:
#line 133 "parse.y" /* yacc.c:1652  */
    { gel_return_ret = TRUE; yyclearin; YYABORT; }
#line 2099 "y.tab.c" /* yacc.c:1652  */
    break;

  case 15:
#line 134 "parse.y" /* yacc.c:1652  */
    { gel_return_ret = TRUE; }
#line 2105 "y.tab.c" /* yacc.c:1652  */
    break;

  case 16:
#line 137 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_SEPAR); }
#line 2111 "y.tab.c" /* yacc.c:1652  */
    break;

  case 17:
#line 138 "parse.y" /* yacc.c:1652  */
    { if ( ! gp_push_local_all ()) {
						SYNTAX_ERROR;
					  }
       					}
#line 2120 "y.tab.c" /* yacc.c:1652  */
    break;

  case 18:
#line 142 "parse.y" /* yacc.c:1652  */
    { if ( ! gp_push_local_idents ()) {
						SYNTAX_ERROR;
					  }
					}
#line 2129 "y.tab.c" /* yacc.c:1652  */
    break;

  case 19:
#line 146 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_MOD_CALC); }
#line 2135 "y.tab.c" /* yacc.c:1652  */
    break;

  case 20:
#line 147 "parse.y" /* yacc.c:1652  */
    { gp_push_null(); PUSH_ACT(GEL_E_SEPAR);
					  gp_push_spacer(); }
#line 2142 "y.tab.c" /* yacc.c:1652  */
    break;

  case 21:
#line 149 "parse.y" /* yacc.c:1652  */
    { gp_push_spacer(); }
#line 2148 "y.tab.c" /* yacc.c:1652  */
    break;

  case 22:
#line 150 "parse.y" /* yacc.c:1652  */
    { mpw_t i;
					  mpw_init (i);
					  mpw_i (i);
					  gp_push_spacer();
					  gel_stack_push(&gel_parsestack,
							 gel_makenum_use(i));
					  PUSH_ACT(GEL_E_MUL); }
#line 2160 "y.tab.c" /* yacc.c:1652  */
    break;

  case 23:
#line 157 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_EQUALS); }
#line 2166 "y.tab.c" /* yacc.c:1652  */
    break;

  case 24:
#line 158 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_DEFEQUALS); }
#line 2172 "y.tab.c" /* yacc.c:1652  */
    break;

  case 25:
#line 159 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_INCREMENT); }
#line 2178 "y.tab.c" /* yacc.c:1652  */
    break;

  case 26:
#line 160 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_INCREMENT_BY); }
#line 2184 "y.tab.c" /* yacc.c:1652  */
    break;

  case 27:
#line 161 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_SWAPWITH); }
#line 2190 "y.tab.c" /* yacc.c:1652  */
    break;

  case 28:
#line 162 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_ABS); }
#line 2196 "y.tab.c" /* yacc.c:1652  */
    break;

  case 29:
#line 163 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_PLUS); }
#line 2202 "y.tab.c" /* yacc.c:1652  */
    break;

  case 30:
#line 164 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_ELTPLUS); }
#line 2208 "y.tab.c" /* yacc.c:1652  */
    break;

  case 31:
#line 165 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_MINUS); }
#line 2214 "y.tab.c" /* yacc.c:1652  */
    break;

  case 32:
#line 166 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_ELTMINUS); }
#line 2220 "y.tab.c" /* yacc.c:1652  */
    break;

  case 33:
#line 167 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_MUL); }
#line 2226 "y.tab.c" /* yacc.c:1652  */
    break;

  case 34:
#line 168 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_ELTMUL); }
#line 2232 "y.tab.c" /* yacc.c:1652  */
    break;

  case 35:
#line 169 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_DIV); }
#line 2238 "y.tab.c" /* yacc.c:1652  */
    break;

  case 36:
#line 170 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_ELTDIV); }
#line 2244 "y.tab.c" /* yacc.c:1652  */
    break;

  case 37:
#line 171 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_BACK_DIV); }
#line 2250 "y.tab.c" /* yacc.c:1652  */
    break;

  case 38:
#line 172 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_ELT_BACK_DIV); }
#line 2256 "y.tab.c" /* yacc.c:1652  */
    break;

  case 39:
#line 173 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_MOD); }
#line 2262 "y.tab.c" /* yacc.c:1652  */
    break;

  case 40:
#line 174 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_ELTMOD); }
#line 2268 "y.tab.c" /* yacc.c:1652  */
    break;

  case 41:
#line 175 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_CMP_CMP); }
#line 2274 "y.tab.c" /* yacc.c:1652  */
    break;

  case 42:
#line 177 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_EQ_CMP); }
#line 2280 "y.tab.c" /* yacc.c:1652  */
    break;

  case 43:
#line 178 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_NE_CMP); }
#line 2286 "y.tab.c" /* yacc.c:1652  */
    break;

  case 44:
#line 179 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_LT_CMP); }
#line 2292 "y.tab.c" /* yacc.c:1652  */
    break;

  case 45:
#line 180 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_GT_CMP); }
#line 2298 "y.tab.c" /* yacc.c:1652  */
    break;

  case 46:
#line 181 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_LE_CMP); }
#line 2304 "y.tab.c" /* yacc.c:1652  */
    break;

  case 47:
#line 182 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_GE_CMP); }
#line 2310 "y.tab.c" /* yacc.c:1652  */
    break;

  case 48:
#line 184 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_LOGICAL_AND); }
#line 2316 "y.tab.c" /* yacc.c:1652  */
    break;

  case 49:
#line 185 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_LOGICAL_OR); }
#line 2322 "y.tab.c" /* yacc.c:1652  */
    break;

  case 50:
#line 186 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_LOGICAL_XOR); }
#line 2328 "y.tab.c" /* yacc.c:1652  */
    break;

  case 51:
#line 187 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_LOGICAL_NOT); }
#line 2334 "y.tab.c" /* yacc.c:1652  */
    break;

  case 52:
#line 192 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_FACT); }
#line 2340 "y.tab.c" /* yacc.c:1652  */
    break;

  case 53:
#line 193 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_DBLFACT); }
#line 2346 "y.tab.c" /* yacc.c:1652  */
    break;

  case 54:
#line 194 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_CONJUGATE_TRANSPOSE); }
#line 2352 "y.tab.c" /* yacc.c:1652  */
    break;

  case 55:
#line 195 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_TRANSPOSE); }
#line 2358 "y.tab.c" /* yacc.c:1652  */
    break;

  case 56:
#line 196 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_NEG); }
#line 2364 "y.tab.c" /* yacc.c:1652  */
    break;

  case 58:
#line 198 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_EXP); }
#line 2370 "y.tab.c" /* yacc.c:1652  */
    break;

  case 59:
#line 199 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_ELTEXP); }
#line 2376 "y.tab.c" /* yacc.c:1652  */
    break;

  case 60:
#line 201 "parse.y" /* yacc.c:1652  */
    {
				if (gp_prepare_push_region_sep ()) {
					PUSH_ACT(GEL_E_REGION_SEP_BY);
				} else {
					PUSH_ACT(GEL_E_REGION_SEP);
				}
					}
#line 2388 "y.tab.c" /* yacc.c:1652  */
    break;

  case 61:
#line 209 "parse.y" /* yacc.c:1652  */
    { /* FIXME: do nothing?, this is just a 
					     get all */ }
#line 2395 "y.tab.c" /* yacc.c:1652  */
    break;

  case 62:
#line 211 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_GET_VELEMENT); }
#line 2401 "y.tab.c" /* yacc.c:1652  */
    break;

  case 63:
#line 212 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_GET_ELEMENT); }
#line 2407 "y.tab.c" /* yacc.c:1652  */
    break;

  case 64:
#line 213 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_GET_ROW_REGION); }
#line 2413 "y.tab.c" /* yacc.c:1652  */
    break;

  case 65:
#line 214 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_GET_ROW_REGION); }
#line 2419 "y.tab.c" /* yacc.c:1652  */
    break;

  case 66:
#line 215 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_GET_COL_REGION); }
#line 2425 "y.tab.c" /* yacc.c:1652  */
    break;

  case 67:
#line 216 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_GET_COL_REGION); }
#line 2431 "y.tab.c" /* yacc.c:1652  */
    break;

  case 68:
#line 217 "parse.y" /* yacc.c:1652  */
    { if(!gp_push_matrix(FALSE)) {SYNTAX_ERROR;} }
#line 2437 "y.tab.c" /* yacc.c:1652  */
    break;

  case 69:
#line 218 "parse.y" /* yacc.c:1652  */
    { if(!gp_push_matrix(TRUE)) {SYNTAX_ERROR;} }
#line 2443 "y.tab.c" /* yacc.c:1652  */
    break;

  case 70:
#line 225 "parse.y" /* yacc.c:1652  */
    {
			if(!gp_push_matrix_row()) {SYNTAX_ERROR;}
			if(!gp_push_marker(GEL_MATRIX_START_NODE)) {SYNTAX_ERROR;}
			if(!gp_push_matrix(TRUE)) {SYNTAX_ERROR;}
					}
#line 2453 "y.tab.c" /* yacc.c:1652  */
    break;

  case 71:
#line 230 "parse.y" /* yacc.c:1652  */
    {SYNTAX_ERROR;}
#line 2459 "y.tab.c" /* yacc.c:1652  */
    break;

  case 72:
#line 231 "parse.y" /* yacc.c:1652  */
    {SYNTAX_ERROR;}
#line 2465 "y.tab.c" /* yacc.c:1652  */
    break;

  case 73:
#line 232 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_WHILE_CONS); }
#line 2471 "y.tab.c" /* yacc.c:1652  */
    break;

  case 74:
#line 233 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_UNTIL_CONS); }
#line 2477 "y.tab.c" /* yacc.c:1652  */
    break;

  case 75:
#line 234 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_DOWHILE_CONS); }
#line 2483 "y.tab.c" /* yacc.c:1652  */
    break;

  case 76:
#line 235 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_DOUNTIL_CONS); }
#line 2489 "y.tab.c" /* yacc.c:1652  */
    break;

  case 77:
#line 236 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_FOR_CONS); }
#line 2495 "y.tab.c" /* yacc.c:1652  */
    break;

  case 78:
#line 237 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_FORBY_CONS); }
#line 2501 "y.tab.c" /* yacc.c:1652  */
    break;

  case 79:
#line 238 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_FORIN_CONS); }
#line 2507 "y.tab.c" /* yacc.c:1652  */
    break;

  case 80:
#line 239 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_SUM_CONS); }
#line 2513 "y.tab.c" /* yacc.c:1652  */
    break;

  case 81:
#line 240 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_SUMBY_CONS); }
#line 2519 "y.tab.c" /* yacc.c:1652  */
    break;

  case 82:
#line 241 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_SUMIN_CONS); }
#line 2525 "y.tab.c" /* yacc.c:1652  */
    break;

  case 83:
#line 242 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_PROD_CONS); }
#line 2531 "y.tab.c" /* yacc.c:1652  */
    break;

  case 84:
#line 243 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_PRODBY_CONS); }
#line 2537 "y.tab.c" /* yacc.c:1652  */
    break;

  case 85:
#line 244 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_PRODIN_CONS); }
#line 2543 "y.tab.c" /* yacc.c:1652  */
    break;

  case 86:
#line 245 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_IF_CONS); }
#line 2549 "y.tab.c" /* yacc.c:1652  */
    break;

  case 87:
#line 246 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_IFELSE_CONS); }
#line 2555 "y.tab.c" /* yacc.c:1652  */
    break;

  case 88:
#line 247 "parse.y" /* yacc.c:1652  */
    { gp_convert_identifier_to_bool ();
					  /* convert true/false to bool */}
#line 2562 "y.tab.c" /* yacc.c:1652  */
    break;

  case 89:
#line 249 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_QUOTE); }
#line 2568 "y.tab.c" /* yacc.c:1652  */
    break;

  case 90:
#line 250 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_REFERENCE); }
#line 2574 "y.tab.c" /* yacc.c:1652  */
    break;

  case 92:
#line 252 "parse.y" /* yacc.c:1652  */
    { gp_push_marker_simple(GEL_EXPRLIST_START_NODE);
					  PUSH_ACT(GEL_E_DIRECTCALL); }
#line 2581 "y.tab.c" /* yacc.c:1652  */
    break;

  case 93:
#line 254 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_DIRECTCALL); }
#line 2587 "y.tab.c" /* yacc.c:1652  */
    break;

  case 94:
#line 255 "parse.y" /* yacc.c:1652  */
    { gp_push_marker_simple(GEL_EXPRLIST_START_NODE);
					  PUSH_ACT(GEL_E_DIRECTCALL); }
#line 2594 "y.tab.c" /* yacc.c:1652  */
    break;

  case 95:
#line 257 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_DIRECTCALL); }
#line 2600 "y.tab.c" /* yacc.c:1652  */
    break;

  case 96:
#line 258 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_CALL); }
#line 2606 "y.tab.c" /* yacc.c:1652  */
    break;

  case 97:
#line 259 "parse.y" /* yacc.c:1652  */
    { gp_push_marker_simple(GEL_EXPRLIST_START_NODE);
					  PUSH_ACT(GEL_E_CALL); }
#line 2613 "y.tab.c" /* yacc.c:1652  */
    break;

  case 98:
#line 261 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_DEFEQUALS); }
#line 2619 "y.tab.c" /* yacc.c:1652  */
    break;

  case 102:
#line 265 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_RETURN); }
#line 2625 "y.tab.c" /* yacc.c:1652  */
    break;

  case 103:
#line 266 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_BAILOUT); }
#line 2631 "y.tab.c" /* yacc.c:1652  */
    break;

  case 104:
#line 267 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_EXCEPTION); }
#line 2637 "y.tab.c" /* yacc.c:1652  */
    break;

  case 105:
#line 268 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_CONTINUE); }
#line 2643 "y.tab.c" /* yacc.c:1652  */
    break;

  case 106:
#line 269 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_BREAK); }
#line 2649 "y.tab.c" /* yacc.c:1652  */
    break;

  case 107:
#line 270 "parse.y" /* yacc.c:1652  */
    { gel_stack_push(&gel_parsestack,
							 gel_makenum_use((yyvsp[0].val))); }
#line 2656 "y.tab.c" /* yacc.c:1652  */
    break;

  case 108:
#line 272 "parse.y" /* yacc.c:1652  */
    { PUSH_CONST_STRING((yyvsp[0].id)); }
#line 2662 "y.tab.c" /* yacc.c:1652  */
    break;

  case 109:
#line 273 "parse.y" /* yacc.c:1652  */
    { gp_push_null(); }
#line 2668 "y.tab.c" /* yacc.c:1652  */
    break;

  case 110:
#line 276 "parse.y" /* yacc.c:1652  */
    { PUSH_ACT(GEL_E_DEREFERENCE); }
#line 2674 "y.tab.c" /* yacc.c:1652  */
    break;

  case 111:
#line 279 "parse.y" /* yacc.c:1652  */
    { PUSH_IDENTIFIER((yyvsp[0].id)); }
#line 2680 "y.tab.c" /* yacc.c:1652  */
    break;

  case 112:
#line 282 "parse.y" /* yacc.c:1652  */
    {
			gp_prepare_push_param (FALSE);
			PUSH_ACT (GEL_E_PARAMETER);
		}
#line 2689 "y.tab.c" /* yacc.c:1652  */
    break;

  case 115:
#line 296 "parse.y" /* yacc.c:1652  */
    {
			if ( ! gp_push_func (FALSE /* vararg */,
					     TRUE /* arguments */,
					     FALSE /* extradict */,
					     FALSE /* never_subst */)) {
				SYNTAX_ERROR;
			}
						}
#line 2702 "y.tab.c" /* yacc.c:1652  */
    break;

  case 116:
#line 304 "parse.y" /* yacc.c:1652  */
    {
			if ( ! gp_push_func (TRUE /* vararg */,
					     TRUE /* arguments */,
					     FALSE /* extradict */,
					     FALSE /* never_subst */)) {
				SYNTAX_ERROR;
			}
							}
#line 2715 "y.tab.c" /* yacc.c:1652  */
    break;

  case 117:
#line 312 "parse.y" /* yacc.c:1652  */
    {
			if ( ! gp_push_func (FALSE /* vararg */,
					     FALSE /* arguments */,
					     FALSE /* extradict */,
					     FALSE /* never_subst */)) {
				SYNTAX_ERROR;
			}
					}
#line 2728 "y.tab.c" /* yacc.c:1652  */
    break;

  case 118:
#line 320 "parse.y" /* yacc.c:1652  */
    {
			if ( ! gp_push_func (FALSE /* vararg */,
					     TRUE /* arguments */,
					     TRUE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
						}
#line 2741 "y.tab.c" /* yacc.c:1652  */
    break;

  case 119:
#line 328 "parse.y" /* yacc.c:1652  */
    {
			if ( ! gp_push_func (TRUE /* vararg */,
					     TRUE /* arguments */,
					     TRUE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
							}
#line 2754 "y.tab.c" /* yacc.c:1652  */
    break;

  case 120:
#line 336 "parse.y" /* yacc.c:1652  */
    {
			if ( ! gp_push_func (FALSE /* vararg */,
					     FALSE /* arguments */,
					     TRUE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
					}
#line 2767 "y.tab.c" /* yacc.c:1652  */
    break;

  case 121:
#line 345 "parse.y" /* yacc.c:1652  */
    {
			if ( ! gp_push_func (FALSE /* vararg */,
					     TRUE /* arguments */,
					     FALSE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
						}
#line 2780 "y.tab.c" /* yacc.c:1652  */
    break;

  case 122:
#line 353 "parse.y" /* yacc.c:1652  */
    {
			if ( ! gp_push_func (TRUE /* vararg */,
					     TRUE /* arguments */,
					     FALSE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
							}
#line 2793 "y.tab.c" /* yacc.c:1652  */
    break;

  case 123:
#line 361 "parse.y" /* yacc.c:1652  */
    {
			if ( ! gp_push_func (FALSE /* vararg */,
					     FALSE /* arguments */,
					     FALSE /* extradict */,
					     TRUE /* never_subst */)) {
				SYNTAX_ERROR;
			}
					}
#line 2806 "y.tab.c" /* yacc.c:1652  */
    break;

  case 125:
#line 372 "parse.y" /* yacc.c:1652  */
    { if(!gp_push_marker(GEL_EXPRLIST_START_NODE)) {SYNTAX_ERROR;} }
#line 2812 "y.tab.c" /* yacc.c:1652  */
    break;

  case 128:
#line 379 "parse.y" /* yacc.c:1652  */
    { if(!gp_push_marker(GEL_EXPRLIST_START_NODE)) {SYNTAX_ERROR;} }
#line 2818 "y.tab.c" /* yacc.c:1652  */
    break;

  case 129:
#line 382 "parse.y" /* yacc.c:1652  */
    { if(!gp_push_matrix_row()) {SYNTAX_ERROR;} }
#line 2824 "y.tab.c" /* yacc.c:1652  */
    break;

  case 130:
#line 383 "parse.y" /* yacc.c:1652  */
    { if(!gp_push_matrix_row()) {SYNTAX_ERROR;} if(!gp_push_marker(GEL_MATRIX_START_NODE)) {SYNTAX_ERROR;} }
#line 2830 "y.tab.c" /* yacc.c:1652  */
    break;


#line 2834 "y.tab.c" /* yacc.c:1652  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 386 "parse.y" /* yacc.c:1918  */

